function [d,D]=TR_subproblem(A,b,e,delta,i)
% function [d,D]=TR_subproblem(A,b,e,delta,i)
%
% Author      : Anahita Hassanzadeh
% Description : practical CG subproblem solver for trust region and trust region with SR1 update
%               To solve Ax=b within the trust region
% Input       : A ~ Hessian
%               b ~ -gradient vector
%               e ~ residual tolerance
%               delta ~  trust region radius
%               i ~ user input structure
%               alg ~ selected algorithm

% Output      : d ~ search direction
%               D ~ directional derivative value

% initialize point
x=zeros((size(b)));

% initialize residual
r0=-b;

% initialize cg vectors
p=-r0;

% initialize iteration counter
k=0;

% store the initial residual
r=r0;

rr=r'*r;

% CG main loop
while 1 
    
    % check initial residual norm for termination
    if norm(r0)<e,
    d=x;
    break;
    end
    
    %evaluate values for calcualtion purposes
    rr0=rr; 
    Ap=A*p;
    pAp=p'*Ap;
    
    % negative curature 
    if pAp<=0
      
        % find a to stop on the radius
        a=roots([p'*p 2*x'*p x'*x-delta^2]);
        a=max(a);
        
        %return direction
        d=x+a*p;
       
        break;
    else
        % O/W update steplength
        a=rr0/pAp;  
    end
    
    %exceeding delta
    if norm(x+a*p)>=delta
        
        % find a to stop on the radius
        a=roots([p'*p 2*x'*p (x'*x-delta^2)]);
        a=max(a);
        
         %return direction
        d=x+a*p;

        break;
    else
        % O/W update the point
        x=x+a*p;   
    end
    
   
    r=r+a*Ap;  % Update residual
    rr=r'*r;
    
    %check new residual norm for cg tolerance
    if norm(r)<e
        d=x;
        %errors='aborted at small residual';
        %fprintf('\n%s\n',errors);
        break;
    end
    
    % terminate if max iteration violated- print error
    if k==i.cgmaxiter
        errors='Aborted at CG subroutine';
        fprintf('%s\n',errors);
        break;
    end
        
    
    beta=(rr)/(rr0); % update beta 
    p=-r+beta*p;     % update cg vector
    k=k+1;           % increment iteration counter
end

 D=-b'*d;            % return directional derivative
        
    
