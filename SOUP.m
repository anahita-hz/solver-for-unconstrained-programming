function x = SOUP(p,x,alg,i)

% function x = SOUP(p,x,o)
%
% Author      : Frank E. Curtis
% Description : SOlver for Unconstrained Programming 
% Input       : p ~ problem function handle
%               x ~ initial point
%               alg ~ selected algorithm
%               i ~ algorithm parameter structure which includes
%               i.opttol: optimality tolerance based on norm of gradient
%               i.maxiter: iteration limit
%               i.c1: sufficient decrease (Armijo) parameter
%               i.c2 : curvature condition parameter in Wolfe l.s.
%               i.backtrackstepratio: step length reduction ratio in
%                                     backtracking l.s
%               i.cgopttol:CG residual tolerance
%               i.cgmaxiter: CG iteration limit
%               i.del0:Initial trust region radius del0
%               i.delhat: trust region radius overall upperbound
%               i.trdirectol: trust region direction acceptance threshold
%                             (0,0.25), 1e-3 suggested
%               i.sr1updatetol: tolerance for BFGS Hessian appr. updates
%               i.damptol: damping parameter for BFGS Hessian appr. updates
% Output      : x ~ final point
% Revised by  : Anahita Hassanzadeh

tic;

% Print out the header, specific for different algorithms
if isempty(strfind(alg,'bfgs')) == 0 
out_line = '==========================================================================================';
out_data = '   k         f         ||g||    updated?   theta      ||d||        g^T*d        alpha';
out_null =                                  '   --   ----------- -----------  ------------  ----------- ';
elseif isempty(strfind(alg,'trust')) == 0 
out_line = '=================================================================================================';
out_data = '   k         f          ||g||          ||d||          D           rho        delta         ||x||';
out_null =                                   '   -----------  ------------  ------------   -----         ------';
else
 out_line = '========================================================================';
 out_data = '   k         f          ||g||        ||d||         g^T*d        alpha';
 out_null =                                  ' -----------  ------------  -----------';
end
fprintf('\n%s\n%s\n%s',out_line,out_data,out_line);


% Initialize iteration counter
k=0;

% Evaluate objective function
f=feval(p,x,0);

% Evaluate objective gradient
g=feval(p,x,1);

% Initialize starting point (this will be used in BFGS and SR1)
x0=x;

% Evaluate gradient of the staring point (to be used in BFGS)
g0=feval(p,x,1);

% Evaluate Hessian (to be used in BFGS)
H=feval(p,x,2);


% Main teration loop
while 1
  
  % Print iterate information
  fprintf('\n%4d  %.4e  %.4e ',k,f,norm(g));
  
  % Check termination conditions
  if k>i.maxiter | norm(g)<=i.opttol, break; end;
  
  % Run direction search to evaluate the new direction, directional derivative, Hessian
  [d,D,H]= directionsearch(p,x,alg,x0,g0,g,i,k,H);
  
  % Print search direction information
  fprintf(' %.4e  %+.4e ',norm(d),D);
  
  % Run stepsearch to evaluate the step length
  [a,f,i.del0,x,x0] = stepsearch(p,x,f,d,D,alg,H,g,i,x0);
  
  % Update iterate
  x=x+a*d;
  
  % Evaluate objective gradient
  g=feval(p,x,1);
  
  % Evaluate gradient of the "old" gradient (to be used in BFGS)
  g0=feval(p,x0,1);
    
  % Increment iteration counter
  k=k+1; 
    
end

% Print output footer
fprintf('%s\n%s\n',out_null,out_line);

out_line = '*******************************';
% Print out the summary of the solution
fprintf('\n     ''Solution Summary''\n %s\n',out_line);
toc;
if k<=i.maxiter 
 fprintf('\nThe problem is solved in %d iterations.\n',k);
 fprintf('The optimal solution is:');Xopt=x'
 else
 fprintf('\nThe maximum number of iterations reached at %d iterations.\n',i.maxiter);  
 fprintf('\nThe final solution obtained is:');Xfinal=x'
end
 fprintf('\nThe function value at the final solution is %e.\n',feval(p,x,0));
 fprintf('\nThe norm of the gradient at the final solution is %e.\n',norm(feval(p,x,1)));










