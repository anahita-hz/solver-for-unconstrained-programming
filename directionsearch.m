function [d,D,H]= directionsearch(p,x,alg,x0,g0,g,i,k,H)
% function [d,D,H]= directionsearch(p,x,alg,x0,g0,g,i,k,H)
%
% Author      : Anahita Hassanzadeh
% Description : Finding direction 
% Input       : p ~ problem function handle
%               x ~ point
%               alg ~ selected algorithm
%               x0 ~ initial point
%               g0 ~ initial gradient vector
%               g ~ gradient vector
%               i ~ input option structure
%               k ~ iteration counter
%               H ~ Hessian matrix
% Output        d ~ search direction
%               D ~ directional derivative value
%               H ~ Hessian matrix


% Steepest descent
if isempty(strfind(alg,'steepest')) == 0
  
  % evaluate gradient
  g = feval(p,x,1);
 
  % Evaluate search direction
  d = -g;
   % Evaluate directional derivative
  D = g'*d;
  
  
 % Newton   
elseif isempty(strfind(alg,'newton')) == 0
    
      % evaluate gradient
      g = feval(p,x,1);

      % ** Evaluate the Hessian
      H = feval(p,x,2);
  
      %** Block of code to ensure H is pd
      n = length(x);
      xi=1e-4;
      while min(eig(H))<0
          H=H+xi*eye(n);
          xi=10*xi;
      end    
  
      % find search direction
      d = H'\(-g);
     
      % Evaluate directional derivative
       D = g'*d;
       
% BFGS         
elseif isempty(strfind(alg,'bfgs')) == 0 
   
   
   % Update the Hessian from the first iteration
   if k>=1
      
     s=x-x0;   % evaluate change in the point location
     Hs=H*s; 
     r=g-g0;   % evaluate change in gradient
     sHs=s'*Hs;
     rs=r'*s;
     
     %Damping starts if bfgs with backtracking line search
     
     % Evaluate theta
     
       if  rs>=i.damptol*sHs % Skip the update if the curvature cond. satisfied
          theta=1;
          update=0; %Updated by theta flag=1
       else
         theta=.8*sHs/(sHs-rs);
         update=1; %Updated by theta flag=1
       end
     
     
     y=theta*r+(1-theta)*Hs; % Update y to ensure rs>=i.damptol*sHs
    
     
     ys=y'*s;
          
     H=H-(Hs*Hs')/(sHs)+(y*y')/(ys); %Update the Hessian (BFGS Update)
     
   
   end
    
   % Ensure the initial Hessian is pd - One possiblility to skip this is to
   % start with Identity matrix
   if k==0        
      % Intialize update flag and theta
      update=0; theta=0;
      %** Block of code to ensure initial H is pd
      n = length(x);
      xi=1e-4;
      while min(eig(H))<0
          H=H+xi*eye(n);
          xi=10*xi;
      end    
  end
  
  
   %find search direction
   d = H'\(-g);
   % Evaluate directional derivative
   D = g'*d; 
   % print iteration information 
   fprintf('%4d  %.4e',update,theta);  


% Trust Region with CG and SR1 Trust Region with CG

elseif isempty(strfind(alg,'trust')) == 0 
    
    if isempty(strfind(alg,'sr1')) == 1  % Evaluate Hessian for trustregioncg
    H=feval(p,x,2);
    
    elseif isempty(strfind(alg,'sr1')) == 0  % For sr1trustregioncg
        
     %evaluate the initial and current gradient
     g0 = feval(p,x0,1);
     g = feval(p,x,1); 
      
     % evaluate the change in point location and gradient
     s=x-x0;
     Hs=H*s;
     y=g-g0;
     yHs=y-Hs;
     yHss=(yHs'*s);
     
   
     % Perform the SR1 update 
     if abs(yHss)>=i.sr1updatetol*norm(yHs)*norm(s) & yHss~=0
      H = H+(yHs*yHs')/(yHss);
     end
   
    end
    
    % for both of SR1-TR and TR-CG run the CG subproblem
    [d,D] = TR_subproblem(H,-g,i.cgopttol,i.del0,i);
   
end
