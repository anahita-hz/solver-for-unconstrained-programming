function x=runner(problem,x0)
% function x = runner(p,x0)
%
% Author      : Frank E. Curtis
% Description : runner for 
%               1. steepest descent alg with backtracking/wolfe line search
%               2. Newton's method with backtracking/wolfe line search
%               3. Trust region method with CG subproblem solver
%               4. SR1 quasi-Newton trust region method with CG subproblem solver
%               5. BFGS quasi-Newton method with backtracking/wolfe line search

% Parameters to be initiated in this file:
%               i.opttol:optimality tolerance based on norm of gradient
%               i.maxiter: iteration limit
%               i.c1: sufficient decrease (Armijo) parameter
%               i.c2 : curvature condition parameter in Wolfe l.s.
%               i.backtrackstepratio: step length reduction ratio in
%                                     backtracking l.s
%               i.cgopttol:CG residual tolerance
%               i.cgmaxiter: CG iteration limit
%               i.del0:Initial trust region radius del0
%               i.delhat: trust region radius overall upperbound
%               i.trdirectol: trust region direction acceptance threshold
%                             (0,0.25)
%               i.sr1updatetol: tolerance for BFGS Hessian appr. updates
%               i.damptol: damping parameter for BFGS Hessian appr. updates

% Input       : problem ~ problem function handle
%               x0 ~ initial point
% Revised by  : Anahita Hassanzadeh

% Set common input parameters for all algorithms
i.opttol = 1e-6;
i.maxiter = 1e+3;

% Set default input parameters for backtracking and wolfe line search
i.c1 = 0.1;
i.c2 = 0.9;

% Set default input parameters for backtracking line search
i.backtrackstepratio=.5;

% Set default input parameters for TR-CG
i.cgopttol= 1e-8;
i.cgmaxiter=5*1e2;

% Initialize trust region radius del0 and trust region radius overal upper bound delhat 
i.del0=1;
i.delhat=90;
%trust region direction acceptance threshold (0,0.25)
i.trdirectol=.24;

% Set default input parameters for TR-CG with SR1 update
i.sr1updatetol=1.0000e-008;

% Set default input parameters for damping BFGS 
i.damptol=0.2000;


out_line = '=================================================================';
out_data = '  Iterations Results for Steepest Descent-Backtraking Line Search ';
fprintf('\n%s\n%s',out_line,out_data);

% Run steepest descent with backtracking line search
x = SOUP(problem,x0,'steepestbacktrack',i);

out_data = '  Iterations Results for Steepest Descent-Wolfe Line Search ';
fprintf('\n%s\n%s',out_line,out_data);

% Run steepest descent with wolfe line search
x = SOUP(problem,x0,'steepestwolfe',i);

out_data = '  Iterations Results for Newton-Backtraking Line Search ';
fprintf('\n%s\n%s',out_line,out_data);

% Run newton with backtracking line search
x = SOUP(problem,x0,'newtonbacktrack',i);

out_data = '  Iterations Results for Newton-Wolfe Line Search ';
fprintf('\n%s\n%s',out_line,out_data);

% Run newton with wolfe line search
x = SOUP(problem,x0,'newtonwolfe',i);

out_data = '  Iterations results for trust region method with CG subproblem solver ';
fprintf('\n%s\n%s',out_line,out_data);

% Run trust region with conjugate gradient subproblem
x = SOUP(problem,x0,'trustregioncg',i);

out_data = '  Iterations results for SR1 quasi-Newton trust region method with CG subproblem solver';
fprintf('\n%s\n%s',out_line,out_data);

% Run TR-CG with SR1 update
x = SOUP(problem,x0,'sr1trustregioncg',i);

out_data = '  Iterations results for  BFGS quasi-Newton method with backtracking line search';
fprintf('\n%s\n%s',out_line,out_data);

% Run damping BFGS with backtracking line search       
 x = SOUP(problem,x0,'bfgsbacktrack',i); 
 
out_data = '  Iterations results for  BFGS quasi-Newton method with wolfe line search';
fprintf('\n%s\n%s',out_line,out_data);

% Run damping BFGS with wolfe search                
 x = SOUP(problem,x0,'bfgswolfe',i);      
         