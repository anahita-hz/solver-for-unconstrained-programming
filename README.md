Supported Algorithms:

               1. Steepest descent alg with backtracking/wolfe line search
               2. Newton's method with backtracking/wolfe line search
               3. Trust region method with CG subproblem solver
               4. SR1 quasi-Newton trust region method with CG subproblem solver
               5. BFGS quasi-Newton method with backtracking/wolfe line search

Start with SOUP.m file. The file runner.m solves the problem with the supported algorithms.

Parameters to be initiated in this file:

               i.opttol:optimality tolerance based on norm of gradient
               i.maxiter: iteration limit
               i.c1: sufficient decrease (Armijo) parameter
               i.c2 : curvature condition parameter in Wolfe l.s.
               i.backtrackstepratio: step length reduction ratio in
                                     backtracking l.s
               i.cgopttol:CG residual tolerance
               i.cgmaxiter: CG iteration limit
               i.del0:Initial trust region radius del0
               i.delhat: trust region radius overall upperbound
               i.trdirectol: trust region direction acceptance threshold
                             (0,0.25)
               i.sr1updatetol: tolerance for BFGS Hessian appr. updates
               i.damptol: damping parameter for BFGS Hessian appr. updates