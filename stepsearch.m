function [a,f,del0,x,x0] = stepsearch(p,x,f,d,D,alg,H,g,i,x0)

% function [a,f,del0,x,x0] = stepsearch(p,x,f,d,D,alg,H,g,i,x0)
%
% Author      : Anahita Hassanzadeh
% Description : stepsearch to evaluate the next step length
% Input       : p ~ problem function handle
%               x ~ current point
%               f ~ function value
%               d ~ search direction
%               D ~ directional derivative value
%               alg ~ selected algorithm
%               H ~ Hessian matrix
%               g ~ gradient vector
%               i ~ input option structure
%               x0 ~ initial point
% Output      : a ~ step length
%               f ~ updated function value
%               del0 ~ previous step trust region radius
%               x ~ current point
%               x0 ~ initial point



 %Initialize trust region radius;
 del0=0;
 
 % Initialize sufficiet decreace parameter (c)
 % Initialize curvature condition parameter in Wolfe l.s.(c2)
 c=i.c1;c2=i.c2;
 
% Backtracking  
if isempty(strfind(alg,'backtrack')) == 0
  % Evaluate unit steplength
  a = 1;
  % Evaluate function value at the initial point
  f0= feval (p,x,0);
  % Evaluate function value
  f = feval(p,x+a*d,0);
  
  % Initialize backward step parameter (rho)
  rho=i.backtrackstepratio;
 
  
  % Loop until Armijo condition satisfied
  while 1
      %check termination condition (Armijo)
      if f<=f0+c*a*D, break;end;
      %Backtrack the step length
      a=a*rho;          
      %Evaluate the updated function value
       f = feval(p,x+a*d,0);
  end
  
  % Store the initial point 
  x0=x; 
  % print the step length
  fprintf(' %.4e',a);
  
  
% Wolfe line search    
elseif isempty(strfind(alg,'wolfe')) == 0
  
  %Initialize the starting step length
  a_old=0;
  %Initialize the maximum step length
  a_max=10;
  %Initialize iteration counter
  i=1;
  %Initialize the step length at iteration 1
  a=1;
  % Evaluate function value at the initial point
  f0= feval (p,x,0);
  
  %Loop until Wolfe cond. satisfied
  while 1
        
     % Evaluate updated function value at the current step
     f = feval(p,x+a*d,0); 
     
     % Evaluate updated function value at the previous step
     f_old=feval(p,x+a_old*d,0);
     
     % Check if Armijo not satisfied or the function value's increased
     if f>f0+c*a*D | (f>=f_old & i>1)
         % Call zoom function to find a step length between a_old and a
         a=zoom(p,x,d,D,a,a_old,c,c2); break;   
     end
     
     % Evaluate obj. gradient at the initial point
     g0=feval(p,x,1); 
     
     % Evaluate obj. gradient at the new point
     g=feval(p,x+a*d,1); 
     
     % Check if strong Wolfe satisfied, break the loop and return a
     if abs(g)<=-c2*g0,break;end;
     
     % Check if headed to a maximizer 
     if g>=0
          % Call zoom function to find a step length between a and a_old
         a=zoom(p,x,d,D,a_old,a,c,c2); break; 
     end
     
     % Store the previous step length
     a_old=a;
     
     %Check the termination condition
     if a<(1-(1e-4))*a_max, break; end;
     
     % Update steplength by bisection
     a=(a_old+a_max)/2;
     
     % Increment counter
     i=i+1;
  end
  
  % Store the initial point- to be used in BFGS
  x0=x; 
  % Print line search information
  fprintf(' %.4e',a);
  
  
  
 % Trust Region and SR1 Trust Region
elseif isempty(strfind(alg,'trust')) == 0
    
    % Evaluate updated function value at the current step
    f=feval(p,x,0);
    % Initialize TR radius
    del0=i.del0;
    
   %%evaluating rho
   % Evaluate the true and predicted obj. at the new point
   f_new=feval(p,x+d,0);
   m_new=f+g'*d+.5*(d'*(H*d));
   
   %evaluate rho
   rho=(f-f_new)/(f-m_new);
   % print rho
   fprintf(' %.4e ',rho);  
    
    
  %changing TR raduis
  
  if rho<=.25             % If the prediction is not good enough:
      del=del0*.25;       % Decrease the TR raduis
  else
         
      if (abs(norm(d)-del0)<=1e-6) & rho>.75  % If prediction is very good
           del=min(2*del0,i.delhat);          % Increase TR raduis
      else                                    
           del=del0;                          % O/W keep the current TR raduis
      end
  end
  
  
 % Accept/Reject direction
 if rho> i.trdirectol
      x0=x;              % If the predictio is good, accept direction
      x=x+d;             % update the point
 end 
 
  a=0;                   % Set the step to zero (to get x=x+d)
  f = feval(p,x,0);      % Evaluate function value at new point
  del0=del;
  fprintf(' %.4e  %.4e',del,norm(x));  %print out information
    
  
end

% Zoom Funtion for Wolfe line search 

function a = zoom(p,x,d,D,a,a_old,c,c2)

% function a = zoom(p,x,d,D,a,a_old,c,c2)
%
% Author      : (Anahita Hassanzadeh)
% Description : Zoom function for Wolfe line search.
% Input       : p ~ problem function handle
%               x ~ point
%               f ~ function value
%               d ~ search direction
%               D ~ directional derivative value
%               a ~ current step length
%               a_old ~ previous step length
%               c ~ Armijo parameter
%               c2 ~ Curvature parameter
% Output      : a ~ step length


%Initialize the min and max of step length
a_hi=a;    a_lo=a_old;


% Loop to guarantee a,b,c of page 61 satisfied
while 1
    
a_int=(a_lo+a_hi)/2; % Start with the middle of the interval

f0= feval(p,x,0);    % evaluate function at the current point

f = feval(p,x+a_int*d,0);   % evaluate function at the middle point

f_lo=feval(p,x+a_lo*d,0);  % evaluate function at the min point

% If the Armijo condition not satisfied or funtion increased
if f>f0+c*a_int*D | f>=f_lo 
    
    a_hi=a_int;   %Switch values to make the interval smaller
    
else
     g0=feval(p,x,1);            % O/W evaluate the gradient at the current point
     g=feval(p,x+a_int*d,1);     % evaluate the gradient at the middle point
 
     if abs(g)<=-c2*g0,a=a_int; break;end; % break out of loop if Wolfe satisfied
     
     if g*(a_hi-a_lo)>=0, a_hi=a_lo; end;  % if only Armijo satisfied
     
     a_lo=a_int;     % switch values
end

% return the middle length if the interval is too small
 if abs(a_hi-a_lo)<1e-4, a=(a_hi+a_lo)/2; break;end; 
end
 
 
